﻿/*EJEMPLO1_4 MODULO1 PAGINA1 ----- N:N CON MULTIVALUADO*/

DROP DATABASE IF EXISTS EJEMPLO1_4;
CREATE DATABASE EJEMPLO1_4;

USE EJEMPLO1_4;

CREATE OR REPLACE TABLE CLIENTES(
  id_c int,
  nombre varchar(50),
  PRIMARY KEY (id_c)
  );

CREATE OR REPLACE TABLE PRODUCTOS(
  id_p int,
  nombre varchar(50),
  peso int,
  PRIMARY KEY (id_p)
  );

CREATE OR REPLACE TABLE TELEFONOS(
  idcliente int,
  telefono int,
  PRIMARY KEY (idcliente, telefono),
  CONSTRAINT fktelefonosclientes FOREIGN KEY (idcliente) REFERENCES CLIENTES (id_c)
  );

CREATE OR REPLACE TABLE COMPRAN(
  idClientes int,
  idProductos int,
  cantidad int,
  fecha date,
  PRIMARY KEY fkcompranclientes FOREIGN KEY (idClientes) REFERENCES CLIENTES (id_c),
  PRIMARY KEY fkcompranprodutos FOREIGN KEY (idProductos) REFERENCES PRODUCTOS (id_p)
  );