﻿/*EJEMPLO1 MODULO1 PAGINA1 ----- N:N*/

DROP DATABASE IF EXISTS EJEMPLO1_1;
CREATE DATABASE EJEMPLO1_1;

USE EJEMPLO1_1;

CREATE OR REPLACE TABLE PRODUCTOS(
  id_p int AUTO_INCREMENT,
  nombre varchar(50),
  peso int,
  PRIMARY KEY (id_p)
  );

CREATE OR REPLACE TABLE CLIENTES(
  id_c int,
  nombre varchar(50),
  PRIMARY KEY (id_c)
  );

CREATE OR REPLACE TABLE COMPRAN(
  idProductos int,
  idClientes int,
  cantidad int, 
  fecha date,
  PRIMARY KEY (idProductos, idClientes),
  CONSTRAINT fkcompranproductos FOREIGN KEY (idProductos) REFERENCES PRODUCTOS (id_p),
  CONSTRAINT fkcompranclientes FOREIGN KEY (idClientes) REFERENCES CLIENTES (id_c)
  );


-- PODEMOS CREAR TABLAS POR UN LADO Y DESPUS REESTRICCIONES
/*
  ALTER TABLE COMPRAN
    ADD CONSTRAINT fkcompranproductos FOREIGN KEY (idProductos) REFERENCES PRODUCTOS (id_p);
    ADD CONSTRAINT fkcompranclientes FOREIGN KEY (idClientes) REFERENCES CLIENTES (id_c);
    
*/


