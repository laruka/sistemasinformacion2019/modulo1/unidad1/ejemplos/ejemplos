﻿/*EJEMPLO1_2 MODULO1 PAGINA1 ----- N:1*/

DROP DATABASE IF EXISTS EJEMPLO1_2;
CREATE DATABASE EJEMPLO1_2;

USE EJEMPLO1_2;

CREATE OR REPLACE TABLE PRODUCTOS(
  id_p int,
  nombre varchar(50),
  peso int,
  PRIMARY KEY (id_p)
  );

CREATE OR REPLACE TABLE CLIENTES(
  id_c int,
  nombre varchar(50),
  PRIMARY KEY (id_c)
  );

CREATE OR REPLACE TABLE COMPRAN(
  idProductos int,
  idClientes int,
  cantidad int, 
  fecha date,
  PRIMARY KEY (idProductos, idClientes),
  UNIQUE KEY (idProductos),
  CONSTRAINT fkcompranproductos FOREIGN KEY (idProductos) REFERENCES PRODUCTOS (id_p),
  CONSTRAINT fkcompranclientes FOREIGN KEY (idClientes) REFERENCES CLIENTES (id_c)
  );

/*modo propagacion*/

DROP DATABASE IF EXISTS EJEMPLO1_2;
CREATE DATABASE EJEMPLO1_2;

USE EJEMPLO1_2;

CREATE OR REPLACE TABLE PRODUCTOS(
  id_p int,
  nombre varchar(50),
  peso int,
  idClientes int,
  cantidad int,
  fecha date,
  PRIMARY KEY (id_p),
  CONSTRAINT fkproductosclientes FOREIGN KEY (idClientes) REFERENCES CLIENTES (id_c)
  );

CREATE OR REPLACE TABLE CLIENTES(
  id_c int,
  nombre varchar(50),
  PRIMARY KEY (id_c)
 );