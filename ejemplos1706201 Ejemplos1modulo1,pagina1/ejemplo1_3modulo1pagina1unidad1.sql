﻿/*EJEMPLO1_3 MODULO1 PAGINA1 ----- 1:1*/

DROP DATABASE IF EXISTS EJEMPLO1_3;
CREATE DATABASE EJEMPLO1_3;

USE EJEMPLO1_3;

CREATE OR REPLACE TABLE CLIENTES(
  id_c int,
  nombre varchar(50),
  PRIMARY KEY (id_c)
  );

CREATE OR REPLACE TABLE PRODUCTOS(
  id_p int,
  nombre varchar(50),
  peso int,
  idClientes int,
  cantidad int,
  fecha date,
  PRIMARY KEY (id_p, idClientes),
  CONSTRAINT fkproductosclientes FOREIGN KEY (idClientes) REFERENCES CLIENTES (id_c)
  );

