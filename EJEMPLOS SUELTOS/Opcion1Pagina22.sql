﻿/*N:N*/

DROP DATABASE IF EXISTS b20190605;
CREATE DATABASE b20190605;
USE b20190605;

/*tambien podemos poner CREATE OR REPLACE para no hacer DROP*/
CREATE TABLE ejemplar(
  cod_ejemplar int,
  PRIMARY KEY (cod_ejemplar)
  );


CREATE TABLE socio(
  cod_socio int,
  PRIMARY KEY (cod_socio)
  );

CREATE TABLE presta(
  ejemplar int,
  socio int,
  fecha_i date,
  fecha_f date,
  PRIMARY KEY (ejemplar, socio),
  CONSTRAINT fkprestaejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (cod_ejemplar),
  CONSTRAINT fkprestasocio FOREIGN KEY (socio) REFERENCES socio (cod_socio) 
  );
