﻿/*EJEMPLO VIDEOCLUB RAMON*/

DROP DATABASE IF EXISTS 20190606_EJERCICIOS;
CREATE DATABASE 20190606_EJERCICIOS;

USE 20190606_EJERCICIOS;

CREATE OR REPLACE TABLE socio(
  codigo varchar(10),
  nombre varchar(50),
  PRIMARY KEY (codigo)
  );


CREATE OR REPLACE TABLE telefonos(
  cod_socio varchar(10),
  telefono int,
  PRIMARY KEY (cod_socio, telefono),
  CONSTRAINT fktelefonossocio FOREIGN KEY (cod_socio) REFERENCES socio (codigo)
  );

CREATE OR REPLACE TABLE correos(
  cod_socio varchar(10),
  email varchar(20),
  PRIMARY KEY (cod_socio, email),
  CONSTRAINT fkcorreossocio FOREIGN KEY (cod_socio) REFERENCES socio (codigo)
  );


CREATE OR REPLACE TABLE peliculas(
  cod_peli varchar(10),
  titulo varchar(40),
  PRIMARY KEY (cod_peli)
  );

CREATE OR REPLACE TABLE alquila(
  codigosocio varchar(10),
  codigopeli varchar(10),
  PRIMARY KEY (codigosocio, codigopeli),
  CONSTRAINT fkalquilasocio FOREIGN KEY (codigosocio) REFERENCES socio (codigo),
  CONSTRAINT fkalquilapeliculas FOREIGN KEY (codigopeli) REFERENCES peliculas (cod_peli)                            
  );

CREATE OR REPLACE TABLE fecha(
  codigosocio varchar(10),
  codpeli varchar(10),
  fecha date,
  PRIMARY KEY (codigosocio, codpeli, fecha),
  CONSTRAINT fkfechasocio FOREIGN KEY (codigosocio) REFERENCES socio (codigo),
  CONSTRAINT fkfechapeliculas FOREIGN KEY (codpeli) REFERENCES peliculas (cod_peli)
  );


