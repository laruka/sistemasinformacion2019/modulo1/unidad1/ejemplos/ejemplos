﻿/*N:1*/

DROP DATABASE IF EXISTS b20190605;
CREATE DATABASE b20190605;
USE b20190605;

CREATE TABLE ejemplar(
  cod_ejemplar int,
  PRIMARY KEY (cod_ejemplar)
  );

CREATE TABLE socio(
  cod_socio int,
  PRIMARY KEY (cod_socio)
  );

CREATE TABLE presta(
  ejemplar int,
  socio int,
  fecha_i int,
  fecha_f int,
  PRIMARY KEY (ejemplar, socio),
  UNIQUE KEY (ejemplar),                        -- aqui le decimos cardinalidad
  CONSTRAINT fkprestaejemplar FOREIGN KEY (ejemplar) REFERENCES ejemplar (cod_ejemplar),
  CONSTRAINT fkprestasocio FOREIGN KEY (socio) REFERENCES socio (cod_socio)
  );


                           
                           
                                
                                
