﻿/*MODULO 1 TEMA 3 PAGINA 2 EJEMPLO*/

DROP DATABASE IF EXISTS 20190605_EJERCICIOS;
CREATE DATABASE 20190605_EJERCICIOS;

USE 20190605_EJERCICIOS;



CREATE OR REPLACE TABLE personas(
  dni varchar(10),
  nombre varchar(50),
  PRIMARY KEY (dni)
  );
 
INSERT INTO personas (dni, nombre)
  VALUES
  ('d1', 'nombre1'),
  ('d2', 'nombre2');






CREATE OR REPLACE TABLE telefonos(
  dni varchar(10),
  numero varchar(12),
  PRIMARY KEY (dni, numero),
  CONSTRAINT fktelefonospersonas FOREIGN KEY (dni) REFERENCES personas (dni)
  );


INSERT INTO telefonos (dni, numero)
  VALUES
  ('d1', 'TLF1'),
  ('d1', 'TLF2'),
  ('d2', 'TLF3');
  
