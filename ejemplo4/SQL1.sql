﻿DROP DATABASE IF EXISTS b20190603;
CREATE DATABASE b20190603;
USE b20190603;


CREATE TABLE coches(
  matricula numeric,
  marca varchar(100),
  modelo varchar(100),
  color varchar(100),
  precio numeric,
  PRIMARY KEY (matricula)
  };

CREATE TABLE cliente(

